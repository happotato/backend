# Simple Supermarket API

## Dependencies

- [.NET 5.0](https://dotnet.microsoft.com/download/dotnet/5.0)
- [PostgreSQL 13](https://www.postgresql.org/download/)

## Setup

```bash
# Restore
dotnet restore

cd Supermercado
```
### Environment

Setup your `PG` connection string on `appsettings.json` or `appsettings.Development.json`.

### Database

```bash
# Install tools
dotnet tool install --global dotnet-ef

# Update database
dotnet ef database update
```

### Running

```bash
# Build
dotnet build

# Start the application
dotnet run
```

### Running with Docker

```bash
# Build
docker build -t supermarket-api .

# Start the application
docker run -d -e ConnectionStrings:PG="<your connection string here>" -p 5000:80 supermarket-api
```
