using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Supermercado.Models
{
    public sealed class Produto : Item
    {
        [MinLength(10)]
        [MaxLength(300)]
        public string Descrição      { get; set; }

        public decimal Preço         { get; set; }

        public int Quantidade        { get; set; }

        public string FornecedorId   { get; set; }

        public Fornecedor Fornecedor { get; set; }
    }
}
