using System;
using System.ComponentModel.DataAnnotations;

namespace Supermercado.Models
{
    public abstract class Item
    {
        [MaxLength(12)]
        public string Id { get; set; }

        public static string GenerateID()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                .Replace('/', '-')
                .Replace('+', '_')
                .Substring(0, 12);
        }
    }
}
