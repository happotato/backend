using System.Collections.Generic;

namespace Supermercado.Models.Providers
{
    public interface IProdutoProvider
    {
        void AdicionarProduto(Produto produto);
        void DeletarProduto(string id);

        Produto ObterProduto(string id);
        IEnumerable<Produto> ObterProdutos();
    }
}
