using System.Collections.Generic;

namespace Supermercado.Models.Providers
{
    public interface IFornecedorProvider
    {
        void AdicionarFornecedor(Fornecedor fornecedor);
        void DeletarFornecedor(string id);

        Fornecedor ObterFornecedor(string id);
        IEnumerable<Fornecedor> ObterFornecedores();
    }
}
