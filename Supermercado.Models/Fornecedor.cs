using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Supermercado.Models
{
    public sealed class Fornecedor : Item
    {
        [MinLength(10)]
        [MaxLength(50)]
        public string Nome            { get; set; }

        [JsonIgnore]
        public List<Produto> Produtos { get; set; }
    }
}
