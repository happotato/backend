﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Supermercado.Models;

namespace Supermercado.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FornecedorController : ControllerBase
    {
        public ApplicationDatabase DB { get; private set; }

        public FornecedorController(ApplicationDatabase db)
        {
            DB = db;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Fornecedor>> ObterFornecedores()
        {
            return Ok(DB.ObterFornecedores());
        }

        [HttpPost]
        public ActionResult<Fornecedor> AdicionarFornecedor([FromBody] JsonElement json)
        {
            String nome = null;

            if (json.TryGetProperty("nome", out JsonElement nomeProperty))
            if (nomeProperty.ValueKind == JsonValueKind.String)
            {
                nome = nomeProperty.GetString();
            }

            var fornecedor = new Fornecedor
            {
                Id = Item.GenerateID(),
                Nome = nome,
            };

            DB.AdicionarFornecedor(fornecedor);
            DB.SaveChanges();

            return Ok(DB.ObterFornecedor(fornecedor.Id));
        }

        [HttpGet("{id}")]
        public ActionResult<Fornecedor> ObterFornecedor(string id)
        {
            return Ok(DB.ObterFornecedor(id));
        }

        [HttpDelete("{id}")]
        public ActionResult DeletarFornecedor(string id)
        {
            var fornecedor = DB.ObterFornecedor(id);

            if (fornecedor.Produtos.Count() > 0 )
            {
                return BadRequest("Produtos > 0");
            }

            DB.DeletarFornecedor(id);
            DB.SaveChanges();

            return Ok();
        }

        [HttpPatch("{id}")]
        public ActionResult<Fornecedor> AtualizarFornecedor(string id, [FromBody] JsonElement json)
        {
            var fornecedor = DB.ObterFornecedor(id);

            if (fornecedor != null)
            {
                if (json.TryGetProperty("nome", out JsonElement nomeProperty))
                if (nomeProperty.ValueKind == JsonValueKind.String)
                {
                    fornecedor.Nome = nomeProperty.GetString();
                }

                DB.SaveChanges();
            }

            return Ok(DB.ObterFornecedor(id));
        }

        [HttpGet("{id}/produtos")]
        public ActionResult<IEnumerable<Produto>> ObterProdutos(string id, string produtoId, string descrição,
            string fornecedorId, decimal preçoMin = 0, decimal preçoMax = Decimal.MaxValue)
        {
            var produtos = DB.ObterProdutos()
                .Where(produto => produto.FornecedorId == id)
                .Where(produto => produtoId == null || produto.Id == produtoId)
                .Where(produto => descrição == null || produto.Descrição == descrição)
                .Where(produto => fornecedorId == null || produto.FornecedorId == fornecedorId)
                .Where(produto => produto.Preço >= preçoMin && produto.Preço <= preçoMax);

            return Ok(produtos);
        }

        [HttpPost("{id}/produtos")]
        public ActionResult<IEnumerable<Produto>> AdicionarProduto(string id, [FromBody] JsonElement json)
        {
            String desc = null;
            decimal preço = 0;
            int quantidade = 0;

            if (json.TryGetProperty("descrição", out JsonElement descProperty) &&
                descProperty.ValueKind == JsonValueKind.String)
            {
                desc = descProperty.GetString();
            }
            else
            {
                return BadRequest();
            }

            if (json.TryGetProperty("preço", out JsonElement preçoProperty) &&
                preçoProperty.ValueKind == JsonValueKind.Number)
            {
                preço = preçoProperty.GetDecimal();
            }
            else
            {
                return BadRequest();
            }

            if (json.TryGetProperty("quantidade", out JsonElement quantidadeProperty) &&
                quantidadeProperty.ValueKind == JsonValueKind.Number)
            {
                quantidade = quantidadeProperty.GetInt32();
            }
            else
            {
                return BadRequest();
            }

            if (quantidade < 1)
                return BadRequest("Quantidade < 1");

            var produto = new Produto 
            {
                Id = Item.GenerateID(),
                Descrição = desc,
                Preço = preço,
                Quantidade = quantidade,
                FornecedorId = id,
            };

            DB.AdicionarProduto(produto);
            DB.SaveChanges();

            return Ok(DB.ObterProduto(produto.Id));
        }
    }
}
