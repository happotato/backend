
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Supermercado.Models;

namespace Supermercado.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        public ApplicationDatabase DB { get; private set; }

        public ProdutoController(ApplicationDatabase db)
        {
            DB = db;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Produto>> ObterProdutos(string id, string descrição, string fornecedorId,
            decimal preçoMin = 0, decimal preçoMax = Decimal.MaxValue)
        {
            var produtos = DB.ObterProdutos()
                .Where(produto => id == null || produto.Id == id)
                .Where(produto => descrição == null || produto.Descrição == descrição)
                .Where(produto => fornecedorId == null || produto.FornecedorId == fornecedorId)
                .Where(produto => produto.Preço >= preçoMin && produto.Preço <= preçoMax);

            return Ok(produtos);
        }

        [HttpGet("{id}")]
        public ActionResult<Produto> ObterProduto(string id)
        {
            return Ok(DB.ObterProduto(id));
        }

        [HttpDelete("{id}")]
        public ActionResult DeletarProduto(string id)
        {
            DB.DeletarProduto(id);
            DB.SaveChanges();

            return Ok();
        }

        [HttpPatch("{id}")]
        public ActionResult<Produto> AtualizarProduto(string id, [FromBody] JsonElement json)
        {
            var produto = DB.ObterProduto(id);

            if (produto != null)
            {
                if (json.TryGetProperty("descrição", out JsonElement descProperty))
                if (descProperty.ValueKind == JsonValueKind.String)
                {
                    produto.Descrição = descProperty.GetString();
                }

                if (json.TryGetProperty("preço", out JsonElement preçoProperty))
                if (preçoProperty.ValueKind == JsonValueKind.Number)
                {
                    produto.Preço = preçoProperty.GetDecimal();
                }

                if (json.TryGetProperty("quantidade", out JsonElement quantidadeProperty))
                if (quantidadeProperty.ValueKind == JsonValueKind.Number)
                {
                    produto.Quantidade = quantidadeProperty.GetInt32();
                }

                DB.SaveChanges();
            }

            return Ok(DB.ObterProduto(id));
        }
    }
}
