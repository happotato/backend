﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuperMercado.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: false),
                    Discriminator = table.Column<string>(type: "text", nullable: false),
                    Nome = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    Descrição = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: true),
                    Preço = table.Column<decimal>(type: "numeric", nullable: true),
                    Quantidade = table.Column<int>(type: "integer", nullable: true),
                    FornecedorId = table.Column<string>(type: "character varying(12)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Item", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Item_Item_FornecedorId",
                        column: x => x.FornecedorId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Item_FornecedorId",
                table: "Item",
                column: "FornecedorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Item");
        }
    }
}
