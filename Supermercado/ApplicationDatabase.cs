using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Supermercado.Models;
using Supermercado.Models.Providers;

namespace Supermercado
{
    public sealed class ApplicationDatabase : DbContext, IFornecedorProvider, IProdutoProvider
    {
        public DbSet<Fornecedor> Fornecedores { get; private set; }
        public DbSet<Produto> Produtos        { get; private set; }

        public ApplicationDatabase(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                .HasKey(item => item.Id);

            modelBuilder.Entity<Produto>()
                .HasOne(produto => produto.Fornecedor)
                .WithMany(fornecedor => fornecedor.Produtos)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired()
                .HasForeignKey(produto => produto.FornecedorId);
        }

        public void AdicionarFornecedor(Fornecedor fornecedor)
        {
            Fornecedores.Add(fornecedor);
        }

        public void DeletarFornecedor(string id)
        {
            var item = Fornecedores
                .Where(f => f.Id == id)
                .ToList()
                .DefaultIfEmpty(null)
                .SingleOrDefault();

            if (item != null)
                Fornecedores.Remove(item);
        }

        public Fornecedor ObterFornecedor(string id)
        {
            return Fornecedores
                .Include(f => f.Produtos)
                .Where(f => f.Id == id)
                .ToList()
                .DefaultIfEmpty(null)
                .SingleOrDefault();
        }

        public IEnumerable<Fornecedor> ObterFornecedores()
        {
            return Fornecedores
                .ToList();
        }

        public void AdicionarProduto(Produto produto)
        {
            Produtos.Add(produto);
        }

        public void DeletarProduto(string id)
        {
            var item = Produtos 
                .Where(f => f.Id == id)
                .ToList()
                .DefaultIfEmpty(null)
                .SingleOrDefault();

            if (item != null)
                Produtos.Remove(item);
        }

        public Produto ObterProduto(string id)
        {
            return Produtos 
                .Include(produto => produto.Fornecedor)
                .Where(f => f.Id == id)
                .ToList()
                .DefaultIfEmpty(null)
                .SingleOrDefault();
        }

        public IEnumerable<Produto> ObterProdutos()
        {
            return Produtos
                .Include(produto => produto.Fornecedor);
        }
    }
}
